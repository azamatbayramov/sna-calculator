from fastapi import FastAPI

from calculator.routes import router as calculator_router

app = FastAPI()

app.include_router(calculator_router)
