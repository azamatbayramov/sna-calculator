from fastapi import APIRouter, HTTPException

from calculator.service import CalculatorService

router = APIRouter()

calculator_service = CalculatorService()


@router.get("/add/{a}/{b}")
async def add(a: float, b: float) -> float:
    result = calculator_service.add(a, b)

    return result


@router.get("/subtract/{a}/{b}")
async def subtract(a: float, b: float) -> float:
    result = calculator_service.subtract(a, b)

    return result


@router.get("/multiply/{a}/{b}")
async def multiply(a: float, b: float) -> float:
    result = calculator_service.multiply(a, b)

    return result


@router.get("/divide/{a}/{b}")
async def divide(a: float, b: float) -> float:
    try:
        result = calculator_service.divide(a, b)
    except ZeroDivisionError as e:
        raise HTTPException(status_code=400, detail=str(e))

    return result
