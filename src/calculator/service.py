class CalculatorService:
    def add(self, a: float, b: float):
        return a + b

    def subtract(self, a: float, b: float):
        return a - b

    def multiply(self, a: float, b: float):
        return a * b

    def divide(self, a: float, b: float):
        if b == 0:
            raise ZeroDivisionError("Cannot divide by zero")
        return a / b
