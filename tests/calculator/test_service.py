import unittest

from src.calculator.service import CalculatorService


class TestCalculatorService(unittest.TestCase):
    def setUp(self):
        self.calculator_service = CalculatorService()

    def test_add(self):
        self.assertEqual(self.calculator_service.add(1.5, 2.5), 4)
        self.assertEqual(self.calculator_service.add(-1.5, 2.5), 1)
        self.assertEqual(self.calculator_service.add(1.5, -2.5), -1)
        self.assertEqual(self.calculator_service.add(-1.5, -2.5), -4)

        self.assertEqual(self.calculator_service.add(0, 1.5), 1.5)
        self.assertEqual(self.calculator_service.add(1.5, 0), 1.5)

        self.assertEqual(self.calculator_service.add(0, 0), 0)

    def test_subtract(self):
        self.assertEqual(self.calculator_service.subtract(1.5, 2.5), -1)
        self.assertEqual(self.calculator_service.subtract(-1.5, 2.5), -4)
        self.assertEqual(self.calculator_service.subtract(1.5, -2.5), 4)
        self.assertEqual(self.calculator_service.subtract(-1.5, -2.5), 1)

        self.assertEqual(self.calculator_service.subtract(1.5, 0), 1.5)
        self.assertEqual(self.calculator_service.subtract(0, 1.5), -1.5)

        self.assertEqual(self.calculator_service.subtract(0, 0), 0)

    def test_multiply(self):
        self.assertEqual(self.calculator_service.multiply(1.5, 2.5), 3.75)
        self.assertEqual(self.calculator_service.multiply(-1.5, 2.5), -3.75)
        self.assertEqual(self.calculator_service.multiply(1.5, -2.5), -3.75)
        self.assertEqual(self.calculator_service.multiply(-1.5, -2.5), 3.75)

        self.assertEqual(self.calculator_service.multiply(1.5, 0), 0)
        self.assertEqual(self.calculator_service.multiply(0, 1.5), 0)

        self.assertEqual(self.calculator_service.multiply(0, 0), 0)

    def test_divide(self):
        self.assertEqual(self.calculator_service.divide(1.5, 2.5), 0.6)
        self.assertEqual(self.calculator_service.divide(-1.5, 2.5), -0.6)
        self.assertEqual(self.calculator_service.divide(1.5, -2.5), -0.6)
        self.assertEqual(self.calculator_service.divide(-1.5, -2.5), 0.6)

        with self.assertRaises(ZeroDivisionError):
            self.calculator_service.divide(1.5, 0)
        self.assertEqual(self.calculator_service.divide(0, 1.5), 0)

        with self.assertRaises(ZeroDivisionError):
            self.calculator_service.divide(0, 0)
